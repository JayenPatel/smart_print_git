<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::namespace('Admin')->group(function () {
	Route::get('/', 'LoginAndRegisterController@Login')->name('admin.login'); 
	Route::get('admin/dashboard', 'DashboardController@index')->name('admin.dashboard');

	//Header
	Route::get('admin/print-job', 'HeaderController@printJob')->name('admin.header.print-job');
	Route::get('admin/calendar', 'HeaderController@calendar')->name('admin.header.calendar');
	Route::get('admin/other-job', 'HeaderController@otherJob')->name('admin.header.other-job');
	Route::get('admin/web-job', 'HeaderController@webJob')->name('admin.header.web-job');
	Route::get('admin/quotes', 'HeaderController@quotes')->name('admin.header.quotes');
	
	// Route::get('admin/account', 'AccountController@index')->name('admin.account');

	//Customer 
    // Route::get('admin/customers','Customers\CustomerController@index')->name('admin.customers.list');
    // Route::get('admin/customers/add','Customers\CustomerController@create')->name('admin.customers.create');
    // Route::get('admin/customers/edit','Customers\CustomerController@edit')->name('admin.customers.edit');

    //task
    // Route::get('admin/tasks','Tasks\TaskController@index')->name('admin.tasks.list');

    //Suppliera
    // Route::get('admin/suppliers/add','Suppliers\SupplierController@create')->name('admin.suppliers.create');
});