<?php

namespace App\Http\Controllers\Admin\Jobs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use log;

class WebJobSheetController extends Controller
{
    public function index(Request $request){
    	try{
    		return view('admin.jobs.web_job_sheet_list');
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function create(){
    	try{
    		return view('admin.jobs.web_job_sheet_create');
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function edit(){
    	try{
    		$customer = 1;
    		return view('admin.jobs.web_job_sheet_create',['customer' => $customer]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
}
