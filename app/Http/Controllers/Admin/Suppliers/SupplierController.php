<?php

namespace App\Http\Controllers\Admin\Suppliers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use log;

class SupplierController extends Controller
{
    public function index(){

    }
    public function create(){
    	try {
    		return view('admin.suppliers.create');
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
}
