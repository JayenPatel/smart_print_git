<?php

namespace App\Http\Controllers\Admin\Staffs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use log;

class StaffController extends Controller
{
    public function index(Request $request){
    	try{
    		return view('admin.staffs.list');
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function create(){
    	try{
    		return view('admin.staffs.create');
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function edit(){
    	try{
    		$customer = 1;
    		return view('admin.staffs.create',['customer' => $customer]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
}
