<?php

namespace App\Http\Controllers\Admin\Customers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use log;

class CustomerController extends Controller
{
    public function index(Request $request){
    	try{
    		// dd('e');
    		return view('admin.customers.list');
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function create(){
    	try{
    		return view('admin.customers.create');
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function edit(){
    	try{
    		$customer = 1;
    		return view('admin.customers.create',['customer' => $customer]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
}
