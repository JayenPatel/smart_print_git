<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;

class LoginAndRegisterController extends Controller
{
    public function Login(Request $request)
    {
    	try {
    		return view('auth.login');
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
}
