<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;

class HeaderController extends Controller
{
    public function printJob(Request $request)
    {
    	try {
    		return view('admin.header.print_job');
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }

    public function calendar(Request $request)
    {
    	try {
    		return view('admin.header.calendar');
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }

    public function otherJob(Request $request)
    {
    	try {
    		return view('admin.header.other_job');
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }

    public function webJob(Request $request)
    {
    	try {
    		return view('admin.header.web_job');
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }

    public function quotes(Request $request)
    {
    	try {
    		return view('admin.header.quotes');
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
}
