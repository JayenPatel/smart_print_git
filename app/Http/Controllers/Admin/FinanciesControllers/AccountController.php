<?php

namespace App\Http\Controllers\Admin\FinanciesControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;

class AccountController extends Controller
{
    public function index(Request $request)
    {
    	try {
    		return view('Financies.Account.account');
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
}
