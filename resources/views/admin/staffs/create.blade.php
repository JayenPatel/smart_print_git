@extends('admin.layouts.app')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class=" container ">

                <h2 class="card-label subject">
                    Staff
                </h2>
            <div class="card card-custom">
				<!--begin::Form-->
				<form>
					<div class="card-body" style="display: flex;">
						<div class="card-body">
							<div class="form-group row">
								<label for="example-first-input" class="col-4 col-form-label">First Name<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Asha @endif" id="example-first-input"/>
								</div>
							</div>
							<div for="example-last-input" class="form-group row">
								<label  class="col-4 col-form-label">Surname<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Negi @endif" id="example-last-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-tel-input" class="col-4 col-form-label">Password<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="password" />
								</div>
							</div>
							<div class="form-group row">
								<label for="example-number-input" class="col-4 col-form-label">Comments :</label>
								<div class="col-8">
									<!-- <input class="form-control input" type="number" value="@if(isset($customer)) 123-342 @endif" id="example-number-input"/> -->
									<textarea class="form-control input" id="exampleTextarea" rows="3">@if(isset($customer)) Hello @endif</textarea>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label for="example-datetime-local-input" class="col-4 col-form-label">Retry Password :</label>
								<div class="col-8">
									<input class="form-control input" type="password" id="example-datetime-local-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-date-input" class="col-4 col-form-label">Start date<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) 12/06/2020 @endif" id="example-date-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-month-input" class="col-4 col-form-label">End date :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) 20/06/2020 @endif" id="example-month-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-week-input" class="col-3 col-form-label">Roles :</label>
								<div class="col-3">
									<div class="checkbox-list">
										<label class="checkbox checkboc_bottom">
						                    <input type="checkbox" @if(isset($customer)) checked @endif /> administrator
						                    <span></span>
						                </label>
						                <label class="checkbox checkboc_bottom">
						                    <input type="checkbox"/> designer
						                    <span></span>
						                </label>
									</div>
								</div>
								<div class="col-3">
									<div class="checkbox-list">
										<label class="checkbox checkboc_bottom">
						                    <input type="checkbox"/> manager
						                    <span></span>
						                </label>
						                <label class="checkbox checkboc_bottom">
						                    <input type="checkbox" @if(isset($customer)) checked @endif/> printer
						                    <span></span>
						                </label>
									</div>
								</div>
								<div class="col-3">
									<div class="checkbox-list">
										<label class="checkbox checkboc_bottom">
						                    <input type="checkbox"/> secretary
						                    <span></span>
						                </label>
						                <label class="checkbox checkboc_bottom">
						                    <input type="checkbox"/> finisher
						                    <span></span>
						                </label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-2">
							</div>
							<div class="col-10">
								<button type="reset" class="btn btn-success mr-2 button_primary_color">Cancel</button>
								<button type="reset" class="btn btn-secondary button_primary_color">Add</button>
							</div>
						</div>
					</div>
				</form>
			</div>
        </div>
        <!--end::Container-->

        <style type="text/css">
        	.checkboc_bottom{
        		margin-bottom: 20px!important;
        		font-size: 16px!important;
        		color: #353B84!important;
        	}
        </style>
@endsection