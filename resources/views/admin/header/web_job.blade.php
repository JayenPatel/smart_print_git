@extends('admin.layouts.app')
@section('content')
<!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                    Web Jobs                            
                </h5>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <style type="text/css">
		th{
			background-color: #353B84 !important;
			color: white !important;
		}
	</style>

<div class="d-flex flex-column-fluid">
<div class="container">
<!--begin::Dashboard-->
	<!--begin::Card-->
	<div class="card card-custom gutter-b">
		<div class="card-header flex-wrap justify-content-center py-3">
			<div class="card-title">
				<h3 class="card-label">
					1 WEB JOBS have been found.
				</h3>
			</div>
		</div>
		<div class="card-body">
			<!--begin: Datatable-->
			<table class="table table-bordered table-checkable" id="kt_datatable">
	            <thead>
					<tr>
						<th>Date</th>
						<th>Customer</th>
						<th>Job Id</th>
						<th>Qty</th>
						<th>Description</th>
						<th>Status</th>
						<th>Type/Design</th>
						<th>Proof</th>
						<th>Confirmed</th>
						<th>Film/Plate</th>
						<th>Job Sheet</th>
						<th>Invoice</th>
						<th>Account</th>
				  	</tr>
	            </thead>
	        
	            <tbody>
	                <tr>
						<td>08 Sep</td>
						<td>Jubraj W3</td>
						<td>122P</td>
						<td>80</td>
						<td>Wall Asia</td>
						<td>D</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td>Go</td>
						<td>new</td>
						<td>ads</td>
	              	</tr>	                
	            </tbody>
	        
    		</table>
			<!--end: Datatable-->
		</div>
	</div>
<!--end::Card-->
</div>
<!--end::Dashboard-->
@endsection