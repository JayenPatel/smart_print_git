@extends('admin.layouts.app')
@section('content')
<!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                    Quotes                            
                </h5>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->

<style type="text/css">
	th{
		background-color: #353B84 !important;
		color: white !important;
	}
</style>

<div class="d-flex flex-column-fluid">
<div class="container">
<!--begin::Dashboard-->
	<!--begin::Card-->
	<div class="card card-custom gutter-b">
		<div class="card-header flex-wrap justify-content-center py-3">
			<div class="card-title">
				<h3 class="card-label">
					5 QUOTES have been found.
				</h3>
			</div>
		</div>
		<div class="card-body">
			<!--begin: Datatable-->
			<table class="table table-bordered table-checkable" id="kt_datatable">
	            <thead>
					<tr>
						<th>Date</th>
						<th>Customer</th>
						<th>Job Id</th>
						<th>Qty</th>
						<th>Description</th>
						<th>Status</th>
						<th>Type/Design</th>
						<th>Proof</th>
						<th>Confirmed</th>
						<th>Film/Plate</th>
						<th>Job Sheet</th>
						<th>Invoice</th>
						<th>Account</th>
				  	</tr>
	            </thead>
	        
	            <tbody>
	                <tr>
						<td>08 Sep</td>
						<td>Spice of India GU12</td>
						<td>1206P</td>
						<td>100</td>
						<td>Bill Book</td>
						<td>D</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td>Go</td>
						<td>new</td>
						<td></td>
	              	</tr>
	              	<tr>
						<td>09 Sep</td>
						<td>Gray of India AU12</td>
						<td>1206P</td>
						<td>800</td>
						<td>Bill Book</td>
						<td>A</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td>Go</td>
						<td>45</td>
						<td></td>
	              	</tr>
	              	<tr>
						<td>05 Jan</td>
						<td>Blue of India AU12</td>
						<td>180AP</td>
						<td>1200</td>
						<td>Bill Book</td>
						<td>B</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td>Go</td>
						<td>20</td>
						<td></td>
	              	</tr>
	              	<tr>
						<td>08 Feb</td>
						<td>Red of India AU12</td>
						<td>55AP</td>
						<td>1299</td>
						<td>Bill Book</td>
						<td>C</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td>Go</td>
						<td>20</td>
						<td></td>
	              	</tr>
	              	<tr>
						<td>12 Feb</td>
						<td>Odd of India AU12</td>
						<td>05SZ</td>
						<td>500</td>
						<td>Bill Book</td>
						<td>A</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable">
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td style="text-align: center;">
							<label class="checkbox checkbox-single">
						    	<input type="checkbox" value="" class="checkable" checked>
						    	<span></span>
							</label>
						</td>
						<td>Go</td>
						<td>20</td>
						<td></td>
	              	</tr>
	                
	            </tbody>
	        
	        		</table>
			<!--end: Datatable-->
		</div>
	</div>
<!--end::Card-->
</div>
<!--end::Dashboard-->
@endsection