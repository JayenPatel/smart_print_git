@extends('admin.layouts.app')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class=" container ">
            <div>
                <h2 class="card-label subject">
                    Tasks
                </h2>
            </div>
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="form-group display_flex">
                        <input type="task" class="form-control task_input" value="Testing TestMe" />
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
@endsection

