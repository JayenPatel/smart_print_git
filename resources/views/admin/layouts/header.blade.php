<!--begin::Wrapper-->
<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
    <!--begin::Header-->
    <div id="kt_header" class="header  header-fixed " >
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <!--begin::Header Menu-->
                <div id="kt_header_menu" class="header-menu header-menu-mobile  header-menu-layout-default " >
                    <!--begin::Header Nav-->
                    <ul class="menu-nav ">
                        <li class="menu-item">
                            <a href="{{ route('admin.header.print-job') }}" class="menu-link header-top-menu">
                                <span class="menu-text"><i class="fa fa-print" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('admin.header.calendar') }}" class="menu-link header-top-menu">
                                <span class="menu-text"><i class="fa fa-calendar" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('admin.header.other-job') }}" class="menu-link header-top-menu">
                                <span class="menu-text"><i class="fa fa-users" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('admin.header.web-job') }}" class="menu-link header-top-menu" style="padding: 17px 21px !important">
                                <span class="menu-text"><i class="fa fa-user" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('admin.header.quotes') }}" class="menu-link header-top-menu">
                                <span class="menu-text"><i class="fa fa-quote-left" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                            </a>
                        </li>
                    </ul>
                    <!--end::Header Nav-->
                </div>
                <!--end::Header Menu-->
            </div>
            <!--end::Header Menu Wrapper-->
            
            <!--begin::Topbar-->
            <div class="topbar">
                <div class="topbar-item">
                    <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                        <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Logout</span>
                    </div>
                </div>
            </div>
            <!--end::Topbar-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Header-->

    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        