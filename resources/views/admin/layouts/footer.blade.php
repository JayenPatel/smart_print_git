<!--begin::Footer-->
<div class="footer py-4 d-flex flex-lg-column " id="kt_footer" style="background: #353B84;">
    <!--begin::Container-->
    <div class=" container-fluid  d-flex flex-column flex-md-row align-items-center justify-content-center">
        <!--begin::Copyright-->
        <div class="text-dark text-dark order-2 order-md-1" >
            <span class="text-muted font-weight-bold mr-2" style="color: white !important;">Copyright &copy; 2020</span>
            <a href="#SmartPrint" class="text-dark-75 text-hover-primary" style="color: white !important;">Smart Print</a>
            <span class="text-muted font-weight-bold mr-2" style="color: white !important;"> | All Rights Reserved.</span>
        </div>
        <!--end::Copyright-->
    </div>
    <!--end::Container-->
</div>
<!--end::Footer-->