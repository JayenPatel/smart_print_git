@extends('admin.layouts.app')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class=" container ">
            <div>
                <h2 class="card-label subject">
                    Printjobsheet
                </h2>
            </div>
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="form-group display_flex">
                        <input type="seach" class="form-control search" />
                        <a href="#" class="btn btn-primary font-weight-bolder button_primary_color" >
                            Search
                        </a>
                    </div>
                </div>
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Dropdown-->
                        <div class="dropdown dropdown-inline mr-2">
                            <form action="{{ route('admin.jobs.print_job_sheets.create') }}" method="get">
                                <button type="submit" class="btn btn-light-primary font-weight-bolder button_primary_color" aria-haspopup="true" aria-expanded="false">
                                    Create
                                </button>
                            </form>
                        </div>
                        <!--end::Dropdown-->

                        <!--begin::Button-->
                        <a href="#" class="btn btn-primary font-weight-bolder button_primary_color">
                            Export To Excel
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table table-bordered table-checkable dataTable no-footer dtr-inline" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Business Name</th>
                                <th>Country</th>
                                <th>Postcode</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Asha</td>
                                <td>Berkshire</td>
                                <td>RG5 4EW</td>
                                <td>
                                    <a href="{{ route('admin.jobs.print_job_sheets.edit')}}"><i class="fa fa-edit fa_fa_icon"></i></a>
                                    <a href=""><i class="fa fa-trash fa_fa_icon"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Asha</td>
                                <td>Berkshire</td>
                                <td>RG5 4EW</td>
                                <td>
                                    <a href="{{ route('admin.jobs.print_job_sheets.edit')}}"><i class="fa fa-edit fa_fa_icon"></i></a>
                                    <a href=""><i class="fa fa-trash fa_fa_icon"></i></a>
                                </td>
                            </tr>
                        </tbody>

                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    
@endsection

