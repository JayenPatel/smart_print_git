@extends('admin.layouts.app')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class=" container ">

                <h2 class="card-label subject">
                    Webjobsheet
                </h2>
            <div class="card card-custom">
				<!--begin::Form-->
				<form>
					<div class="card-body">
						<div class="card-body">
							<div class="form-group row">
								<label class="col-3 col-form-label lable_highlight"><strong>Job Id: </strong></label>
								<label class="col-3 col-form-label lable_highlight"><strong>Reg.Date: </strong></label>
							</div>
						</div>
						<div style="display: flex;">
							<div class="card-body">
								<div class="form-group row">
									<label  class="col-4 col-form-label">Category: </label>
									<div class="col-6">
										<select class="form-control input">
											<option>Choose one</option>
											<option @if(isset($customer)) selected @endif>IT</option>
										</select>
									</div>
									<div class="col-1"><button class="plus_minus_button">+</button></div>
									<div class="col-1"><button class="plus_minus_button">-</button></div>
								</div>
								<div class="form-group row">
									<label for="example-search-input" class="col-4 col-form-label">Description: </label>
									<div class="col-8">
										<input class="form-control input" type="text" value="@if(isset($customer)) Develop @endif" id="example-search-input"/>
									</div>
								</div>
								<div class="highlight">
									<div class="form-group row">
										<label for="example-email-input" class="col-4 col-form-label">Quantity: </label>
										<div class="col-3">
											<input class="form-control input_hightlight" type="text" value="@if(isset($customer)) Asha @endif" id="example-email-input"/>
										</div>
									</div>
									<div class="form-group row">
										<label  class="col-4 col-form-label">VAT: </label>
										<div class="col-3">
											<input class="form-control input_hightlight" type="text" value="17.5%" id="example-text-input"/>
										</div>
									</div>
									<div class="form-group row">
										<label for="example-tel-input" class="col-4 col-form-label">Price Comments: </label>
										<div class="col-8">
											<textarea class="form-control input_hightlight" id="exampleTextarea" rows="3">@if(isset($customer)) Hello @endif</textarea>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-number-input" class="col-4 col-form-label">URL: </label>
									<div class="col-8">
										<input class="form-control input" type="number" value="@if(isset($customer)) 123-342 @endif" id="example-number-input"/>
									</div>
								</div>
							</div>
							<div class="card-body">
								<div class="form-group row">
									<label for="example-color-input" class="col-3 col-form-label">Status: </label>
									<div class="col-8">
										<select class="form-control input">
											<option>Select Status</option>
											<option @if(isset($customer)) selected @endif>Active</option>
											<option>InActive</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-datetime-local-input" class="col-3 col-form-label">Quantity: </label>
									<div class="col-8">
										<input class="form-control input" type="text" value="@if(isset($customer)) www.shineinfo.com @endif" id="example-datetime-local-input"/>
									</div>
								</div>
								<div class="highlight">
									<div class="form-group row lable_highlight_margin_null">
										<label for="example-date-input" class="col-3 col-form-label lable_highlight"><strong>EXTRAS</strong></label>
									</div>
									<div class="form-group row">
										<label for="example-date-input" class="col-3 col-form-label">Description: </label>
										<div class="col-8">
											<input class="form-control input_hightlight" type="text" value="@if(isset($customer)) E 134 @endif" id="example-date-input"/>
										</div>
									</div>
									<div class="form-group row">
										<label for="example-month-input" class="col-3 col-form-label">VAT:</label>
										<div class="col-3">
											<input class="form-control input_hightlight" type="text" value="17.5%" id="example-month-input"/>
										</div>
									</div>
									<div class="form-group row">
										<label for="example-week-input" class="col-3 col-form-label">Net_Price:</label>
										<div class="col-3">
											<input class="form-control input_hightlight" type="text" value="@if(isset($customer)) Corn @endif" id="example-week-input"/>
										</div>
										<div>
											<button class="btn btn-success mr-2 button_primary_color">Add Extra</button>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label for="example-time-input" class="col-3 col-form-label">Ftp: </label>
									<div class="col-8">
										<input class="form-control input" type="text" value="@if(isset($customer)) Udaipur @endif" id="example-time-input"/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-2">
							</div>
							<div class="col-10">
								<button type="reset" class="btn btn-success mr-2 button_primary_color">Add</button>
								<button type="reset" class="btn btn-secondary button_primary_color">Cancel</button>
							</div>
						</div>
					</div>
				</form>
			</div>
        </div>
        <!--end::Container-->
@endsection