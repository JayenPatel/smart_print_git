@extends('admin.layouts.app')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class=" container ">

                <h2 class="card-label subject">
                    Suppliers
                </h2>
            <div class="card card-custom">
				<!--begin::Form-->
				<form>
					<div class="card-body" style="display: flex;">
						<div class="card-body">
							<div class="form-group row">
								<label  class="col-4 col-form-label">Supplier Type<span>*</span> :</label>
								<div class="col-8">
									<select class="form-control input">
										<option>Choose one</option>
										<option @if(isset($customer)) selected @endif>IT</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-search-input" class="col-4 col-form-label">Supplier Name<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Develop @endif" id="example-search-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-email-input" class="col-4 col-form-label">First Name<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Asha @endif" id="example-email-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label  class="col-4 col-form-label">Last Name<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Negi @endif" id="example-text-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-tel-input" class="col-4 col-form-label">Telephone<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="tel" value="@if(isset($customer)) 12365478 @endif" id="example-tel-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-number-input" class="col-4 col-form-label">Fax :</label>
								<div class="col-8">
									<input class="form-control input" type="number" value="@if(isset($customer)) 123-342 @endif" id="example-number-input"/>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label for="example-datetime-local-input" class="col-3 col-form-label">Web :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) www.shineinfo.com @endif" id="example-datetime-local-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-date-input" class="col-3 col-form-label">Address1<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) E 134 @endif" id="example-date-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-month-input" class="col-3 col-form-label">Address2 :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Sector @endif" id="example-month-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-week-input" class="col-3 col-form-label">Area :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Corn @endif" id="example-week-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-time-input" class="col-3 col-form-label">Town<span>*</span> :</label>
								<div class="col-8">
									<input class="form-control input" type="text" value="@if(isset($customer)) Udaipur @endif" id="example-time-input"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-color-input" class="col-3 col-form-label">Country<span>*</span> :</label>
								<div class="col-8">
									<select class="form-control input">
										<option>Choose one</option>
										<option @if(isset($customer)) selected @endif>India</option>
										<option>America</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-2">
							</div>
							<div class="col-10">
								<button type="reset" class="btn btn-success mr-2 button_primary_color">Submit</button>
								<button type="reset" class="btn btn-secondary button_primary_color">Cancel</button>
							</div>
						</div>
					</div>
				</form>
			</div>
        </div>
        <!--end::Container-->
@endsection