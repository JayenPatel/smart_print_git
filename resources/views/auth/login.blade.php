<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="{{ asset('css/admin.min.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<section class="admin-login">
		<div class="row justify-content-center">
		  <div class="box__">
		    <div class="login-logo">
		    	<img src="{{ asset('authImage/SPIC_logo.png')}}" style="width: 100%;">
		    </div>
		    <div style="margin: 60px 0 55px 0">
		    	<form method="get" action="{{ route('admin.dashboard') }}">
		    		<div class="login-input-box">
				    	<i class="fa fa-envelope" aria-hidden="true"></i>
				    	<input type="email" name="email" placeholder="Enter Email" class="login-input">
				    </div>
				    <div class="login-input-box">
				    	<i class="fa fa-lock" aria-hidden="true" style="font-size: 24px;"></i>
				    	<input type="password" name="password" placeholder="Enter Password" class="login-input">
				    </div>
				    <div class="text-center" style="margin-top: 35px;">
				    	<button type="submit" class="login_btn">Login</button>
				    </div>
		    	</form>
		    </div>
		  </div>
		</div>
	</section>	
</body>
</html>